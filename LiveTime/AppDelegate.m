//
//  AppDelegate.m
//  LiveTime
//
//  Created by Jaehoon Lee on 2014. 5. 9..
//  Copyright (c) 2014년 DevTeam. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "QuestionsViewController.h"
#import "ResultViewController.h"
#import <KakaoOpenSDK/KakaoOpenSDK.h>


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(kakaoSessionDidChangeWithNotification:) name:KOSessionDidChangeNotification object:nil];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // Launch Images를 보여주는 시간을 지연시킨다
    [NSThread sleepForTimeInterval:2.0];
    
    // 앱을 첫 실행한 경우 사용자 값에 초기값을 저장한다
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    
    if (nil == [userData valueForKey:@"FirstVisit"]) {
        [userData setValue:[NSNumber numberWithInteger:1] forKey:@"FirstVisit"];
        [userData synchronize];
    }
    
    UIStoryboard *storyboard = nil;
    UIViewController *initialViewController = nil;
    
    // 기기의 해상도를 판별한다
    if (IS_4_INCH_DEVICE) {
        storyboard = [UIStoryboard storyboardWithName:@"Main_4inch" bundle:nil];
    }
    else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_3.5inch" bundle:nil];
    }
    
    // 앱을 첫 실행한 경우 문제 화면으로 이동한다
    if (1 == [[userData valueForKey:@"FirstVisit"] intValue]) {
        initialViewController = [storyboard instantiateViewControllerWithIdentifier:@"QuestionVC"];
        self.window.rootViewController = initialViewController;
    }
    
    // 앱을 실행하여 문제를 풀었던 적이 있을 경우 결과 화면으로 바로 이동한다
    if (0 == [[userData valueForKey:@"FirstVisit"] intValue]) {
        initialViewController = [storyboard instantiateViewControllerWithIdentifier:@"ResultVC"];
        self.window.rootViewController = initialViewController;
    }
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [KOSession handleDidBecomeActive];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([KOSession isKakaoAccountLoginCallback:url]) {
        return [KOSession handleOpenURL:url];
    }
    return YES;
}

// 카카오 세션 상태가 변경되면
- (void)kakaoSessionDidChangeWithNotification:(NSNotification *)notification
{
    if (![[KOSession sharedSession] isOpen]) {
        NSLog(@"로그아웃 되었습니다.");
    }
    else {
        NSLog(@"로그인 되었습니다.");
    }
}

@end
