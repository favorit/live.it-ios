//
//  ResultViewController.m
//  LiveTime
//
//  Created by Jaehoon Lee on 2014. 5. 10..
//  Copyright (c) 2014년 DevTeam. All rights reserved.
//

#import "ResultViewController.h"
#import "QuestionsViewController.h"
#import <KakaoOpenSDK/KakaoOpenSDK.h>
#import <MessageUI/MessageUI.h>
#import "DACircularProgressView.h"
//#import "MobileAdView.h"
#import "GADBannerView.h"

#define ADMOB_BANNER_UNIT_ID    @"ca-app-pub-9945504005389323/7810354694";      // 구글 애드몹 광고 유닛 아이디

@interface ResultViewController () <MFMailComposeViewControllerDelegate, UIAlertViewDelegate>
{
    GADBannerView *_adBannerView;   // 구글 애드몹
//    MobileAdView *_adView;        // 네이버 애드포스트
}

@property (weak, nonatomic) IBOutlet UILabel *expectedAgeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *percentLabel;

@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@end


@implementation ResultViewController
{
    NSUserDefaults *_userData;  // 사용자 값
    NSInteger _expectedAge;     // 기대수명
    
    NSString *_years;           // 남은 수명(연)
    NSString *_months;          // 남은 수명(월)
    NSString *_days;            // 남은 수명(일)    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showAdMobBanner];
//    [self showAdPostBanner];
    
    // 사용자 값을 읽어온다
    _userData = [NSUserDefaults standardUserDefaults];
    _expectedAge = [[_userData valueForKey:@"ExpectedAge"] integerValue];   // 기대수명
    NSDate *deathDay = [_userData valueForKey:@"DeathDay"];                 // 사망일자
    NSDate *today = [NSDate date];
    
    // 기대수명을 화면에 출력한다
    self.expectedAgeLabel.frame = (100 > _expectedAge) ? CGRectMake(69, 92, 134, 95) : CGRectMake(93, 92, 134, 95);
    self.ageLabel.frame = (100 > _expectedAge) ? CGRectMake(203, 148, 40, 33) : CGRectMake(230, 148, 40, 33);
    self.expectedAgeLabel.text = [NSString stringWithFormat:@"%ld", (long)_expectedAge];
    
    // 살아 온 기간을 백분율(%)로 계산한다
    NSInteger totalDays = [[_userData valueForKey:@"ExpectedAge"] integerValue] * 365;      // 총 수명(일)
    double remainDays = [deathDay timeIntervalSinceNow] / 60 / 60 / 24;                     // 남은 수명(일)
    float percent = (float)remainDays / totalDays;
    
    // progressLabel에 퍼센트를 표시한다
    self.percentLabel.text = [NSString stringWithFormat:@"%.0f", floor(percent * 100)];

    // 화면 하단의 라벨에 남은 수명을 표시한다
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unit = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *comp = [calendar components:unit fromDate:today toDate:deathDay options:0];
    [self presentLabels:remainDays year:comp];

    // 백분율에 따라 메세지를 표시한다
    [self messagePresent:percent];
    
    // 화면 상단에 원형 그래프를 그린다
    [self drawCircularProgress:percent];
}

// 구글 애드몹 광고배너를 띄운다
- (void)showAdMobBanner
{
    _adBannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, 518, 320, 50)];
    _adBannerView.adUnitID = ADMOB_BANNER_UNIT_ID;
    _adBannerView.rootViewController = self;
    [self.view addSubview:_adBannerView];
    
    GADRequest *request = [GADRequest request];
//    request.testDevices = [NSArray arrayWithObject:@"d07aede00422e4aeed9eb93ebeeab19f"];
    
    [_adBannerView loadRequest:request];
}

/*
// 네이버 애드포스트 광고배너를 띄운다
- (void)showAdPostBanner
{
    _adView = [MobileAdView sharedMobileAdView];
    
    [_adView setFrame:CGRectMake(0, 518, 320, 50)];
    [_adView setSuperViewController:self];
    [_adView setChannelId:@"mios_925f0588267343d5a85ae3c11f7a9a1d"];
    [_adView setIsTest:YES];
    
    [_adView setDelegate:self];
    [self.view addSubview:_adView];
    
    [_adView start];
}

- (void)adDidReceived:(MobileAdErrorType)err{
    // 오류 처리 코드
    
    if (err == 0) {
        NSLog(@"애드포스트 광고 요청 성공");
    }
    else
        NSLog(@"애드포스트 광고 요청 실패 : %d", err);
}
*/

- (void)drawCircularProgress:(float)percent
{
    // 원형 그래프를 그린다
    DACircularProgressView *progressView = [[DACircularProgressView alloc] initWithFrame:CGRectMake(55, 75, 210, 210)];
    progressView.roundedCorners = NO;
    progressView.progressTintColor = [UIColor redColor];
    [progressView setTrackTintColor:[[UIColor blackColor] colorWithAlphaComponent:0.1]];
    progressView.progress = percent;
    progressView.thicknessRatio = 0.06;
    progressView.clockwiseProgress = 1;
    
    [self.view addSubview:progressView];
}

// percent의 값에 따라 화면 중앙에 메세지를 띄운다
- (void)messagePresent:(float)percent
{
    percent = floor(percent *100);
    percent = 95;
        
    if (percent <= 10) {
        self.messageLabel.text = NSLocalizedString(@"RESULT_MESSAGE_10", @"결과 메세지10");
    }
    else if (11 <= percent && percent <= 20) {
        self.messageLabel.text = NSLocalizedString(@"RESULT_MESSAGE_9", @"결과 메세지9");
    }
    else if (21 <= percent && percent <= 30) {
        self.messageLabel.text = NSLocalizedString(@"RESULT_MESSAGE_8", @"결과 메세지8");
    }
    else if (31 <= percent && percent <= 40) {
        self.messageLabel.text = NSLocalizedString(@"RESULT_MESSAGE_7", @"결과 메세지7");
    }
    else if (41 <= percent && percent <= 50) {
        self.messageLabel.text = NSLocalizedString(@"RESULT_MESSAGE_6", @"결과 메세지6");
    }
    else if (51 <= percent && percent <= 60) {
        self.messageLabel.text = NSLocalizedString(@"RESULT_MESSAGE_5", @"결과 메세지5");
    }
    else if (61 <= percent && percent <= 70) {
        self.messageLabel.text = NSLocalizedString(@"RESULT_MESSAGE_4", @"결과 메세지4");
    }
    else if (71 <= percent && percent <= 80) {
        self.messageLabel.text = NSLocalizedString(@"RESULT_MESSAGE_3", @"결과 메세지3");
    }
    else if (81 <= percent && percent <= 90) {
        self.messageLabel.text = NSLocalizedString(@"RESULT_MESSAGE_2", @"결과 메세지2");
    }
    else if (91 <= percent && percent <= 100) {
        self.messageLabel.text = NSLocalizedString(@"RESULT_MESSAGE_1", @"결과 메세지1");
    }
}

// 화면 하단의 라벨에 남은 수명을 표시한다
- (void)presentLabels:(NSInteger)diffDays year:(NSDateComponents *)comp
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    _years = [formatter stringFromNumber:[NSNumber numberWithInteger: comp.year]];
    self.yearLabel.text = _years;
    
    _months = [formatter stringFromNumber:[NSNumber numberWithInteger: comp.year*12 + comp.month]];
    self.monthLabel.text = _months;
    
    _days = [formatter stringFromNumber:[NSNumber numberWithInteger: diffDays]];
    self.dayLabel.text = _days;
}

// 다시 계산하기 버튼을 누르면 첫 질문 화면으로 이동한다
- (IBAction)restartQuestions:(id)sender
{
    // 사용자 값의 첫 방문 여부 값을 YES로 변경한다
    [_userData setValue:[NSNumber numberWithInteger:1] forKey:@"FirstVisit"];
    [_userData synchronize];
    
    // 첫 질문 페이지로 이동한다
    UIStoryboard *storyboard = self.storyboard;
    QuestionsViewController *questionVC = [storyboard instantiateViewControllerWithIdentifier:@"QuestionViewController"];
    questionVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:questionVC animated:YES completion:nil];
}

// 사용자가 피드백 버튼을 누르면 메일 발송 화면이 나타난다
- (IBAction)feedbackButtonPressed:(id)sender
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
        [mailVC setMailComposeDelegate:self];
        
        [mailVC setSubject:NSLocalizedString(@"EMAIL_TITLE", @"이메일 제목")];
        [mailVC setToRecipients:[NSArray arrayWithObjects:@"mayaljh@gmail.com", @"drexdream@naver.com", nil]];
        
        NSString *messageBody = NSLocalizedString(@"EMAIL_BODY", @"이메일 본문");
        [mailVC setMessageBody:messageBody isHTML:NO];
        [self presentViewController:mailVC animated:YES completion:nil];
    }
}

// 메일 발송 결과를 처리한다
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Result : canceled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Result : saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Result : sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Result : failed");
            break;
        default:
            NSLog(@"Wrong Result!");
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:nil];
}

// 카카오톡 공유 버튼을 누르면
- (IBAction)kakaoTalkButtonPressed:(id)sender
{
    // 기기의 로그인 수행 가능한 카카오 앱에 로그인 요청을 전달한다
    [[KOSession sharedSession] openWithCompletionHandler:^(NSError *error) {

        // 카카오 인증이 되었으면
        if ([[KOSession sharedSession] isOpen]) {
            NSLog(@"로그인 성공(Access Token : %@)", [KOSession sharedSession].accessToken);
            
            // 카카오톡의 프로필 정보를 가져온다
            [KOSessionTask talkProfileTaskWithCompletionHandler:^(KOTalkProfile* result, NSError* error) {
                if (result) {
                    NSLog(@"사용자 정보 요청 성공");
                    
                    // 카카오톡이 설치되어 있는 경우
                    if (YES == [KOAppCall canOpenKakaoTalkAppLink]) {
                        // 메세지를 전송한다
                        NSString *profileName = result.nickName;
                        NSString *message = [NSString stringWithFormat:NSLocalizedString(@"KAKAOTALK_MESSAGE", @"카카오톡 메세지"), profileName, _expectedAge, _years, _months, _days];
                        KakaoTalkLinkObject *label = [KakaoTalkLinkObject createLabel:message];
                    
                        // 앱 링크 버튼을 전송한다
                        KakaoTalkLinkAction *androidAppAction = [KakaoTalkLinkAction createAppAction:KakaoTalkLinkActionOSPlatformAndroid devicetype:KakaoTalkLinkActionDeviceTypePhone execparam:nil];
                        KakaoTalkLinkAction *iphoneAppAction = [KakaoTalkLinkAction createAppAction:KakaoTalkLinkActionOSPlatformIOS devicetype:KakaoTalkLinkActionDeviceTypePhone execparam:nil];
                        KakaoTalkLinkObject *button = [KakaoTalkLinkObject createAppButton:NSLocalizedString(@"KAKAOTALK_LINK", @"카카오톡 링크") actions:@[androidAppAction, iphoneAppAction]];
                    
                        [KOAppCall openKakaoTalkAppLink:@[label, button]];
                    }
                    
                    // 카카오톡이 설치되어 있지 않은 경우
                    else {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", @"알림 제목") message:NSLocalizedString(@"KAKAOTALK_ALERT_MESSAGE", @"카카오톡 알림 메세지") delegate:self cancelButtonTitle:nil otherButtonTitles: NSLocalizedString(@"ALERT_BUTTON", @"알림 버튼"), nil];
                        alert.tag = 1;
                        [alert show];

                        NSLog(@"카카오톡 앱이 설치되어 있지 않습니다.");
                    }
                }
                else {
                    NSLog(@"사용자 정보 요청 실패");
                }
            }];
        }
        else {
            NSLog(@"로그인 실패");
        }
    }];
}

-(BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    if (alertView.tag == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/kakaotalk-messenger/id362057947?mt=8"]];
    }
    return YES;
}

// 카카오스토리 공유 버튼을 누르면
- (IBAction)kakaoStoryButtonPressed:(id)sender
{
    // 기기의 로그인 수행 가능한 카카오 앱에 로그인 요청을 전달한다
    [[KOSession sharedSession] openWithCompletionHandler:^(NSError *error) {

        // 카카오 인증이 되었으면
        if ([[KOSession sharedSession] isOpen]) {
            NSLog(@"로그인 성공(Access Token : %@)", [KOSession sharedSession].accessToken);
            
            // 카카오스토리의 프로필 정보를 가져온다
            [KOSessionTask storyProfileTaskWithCompletionHandler:^(KOStoryProfile* profile, NSError* error) {
                if (profile) {
                    NSLog(@"카카오스토리 사용자 정보 요청 성공");

                    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"KAKAOTALK_MESSAGE", @"카카오스토리 메세지"), profile.nickName, _expectedAge, _years, _months, _days];

                    // 카카오스토리에 기대수명을 글 방식으로 포스팅 한다
                    [KOSessionTask storyPostNoteTaskWithContent:message permission:KOStoryPostPermissionFriend sharable:YES
                                               androidExecParam:@{@"andParam1":@"value1",@"andParam2":@"value2"}
                                                   iosExecParam:@{@"iosParam1":@"value1",@"iosParam2":@"value2"}
                                              completionHandler:^(KOStoryPostInfo *post, NSError *error) {
                                                  if (!error) {
                                                      NSLog(@"카카오스토리 글 포스팅 성공 : postId=%@", post.ID);
                                                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", @"알림 제목") message:NSLocalizedString(@"KAKAOSTORY_ALERT_SUCCESS_MESSAGE", @"카카오스토리 성공 알림 메세지") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BUTTON", @"알림 버튼") otherButtonTitles: nil];
                                                      [alert show];
                                                  } else {
                                                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", @"알림 제목") message:NSLocalizedString(@"KAKAOSTORY_ALERT_FAIL_MESSAGE", @"카카오스토리 실패 알림 메세지") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BUTTON", @"알림 버튼") otherButtonTitles: nil];
                                                      [alert show];
                                                  }
                                              }];
                    

                    // 카카오스토리에 기대수명을 링크 방식으로 포스팅 한다
                    [KOSessionTask storyGetLinkInfoTaskWithUrl:@"https://itunes.apple.com/us/app/live.it/id897534417?mt=8"
                                             completionHandler:^(KOStoryLinkInfo *link, NSError *error) {
                                                 if (!error) {
                                                     // 링크 정보 요청 성공
                                                     // post a link
                                                     [KOSessionTask storyPostLinkTaskWithLinkInfo:link
                                                                                          content:message
                                                                                       permission:KOStoryPostPermissionFriend
                                                                                         sharable:YES
                                                                                 androidExecParam:@{@"andParam1":@"value1",@"andParam2":@"value2"}
                                                                                     iosExecParam:@{@"iosParam1":@"value1",@"iosParam2":@"value2"}
                                                                                completionHandler:^(KOStoryPostInfo *post, NSError *error) {
                                                                                    if (!error) {
                                                                                        NSLog(@"링크 포스팅 성공(postId=%@)", post.ID);
                                                                                    } else {
                                                                                        NSLog(@"링크 포스팅 실패");
                                                                                    }
                                                                                }];
                                                 } else {
                                                     // 링크 정보 요청 실패
                                                     NSLog(@"failed to get a link info.");
                                                 }
                                             }];
                } else {
                    NSLog(@"카카오스토리 사용자 정보 요청 실패");
                }
            }];
        }
        else {
            NSLog(@"로그인 실패");
        } 
    }];
}

// 상태바의 글씨 색상을 설정한다
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
