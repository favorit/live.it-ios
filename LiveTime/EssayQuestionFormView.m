//
//  EssayQuestionFormView.m
//  LiveTime
//
//  Created by Jaehoon Lee on 2014. 5. 16..
//  Copyright (c) 2014년 DevTeam. All rights reserved.
//

#import "EssayQuestionFormView.h"

@implementation EssayQuestionFormView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        toolbar.barStyle = UIBarStyleBlack;
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(handleDone:)];
        [toolbar setItems:[NSArray arrayWithObject:doneButton]];
        self.heightTextField.inputAccessoryView = toolbar;
        self.weightTextField.inputAccessoryView = toolbar;
        
        self.heightTextField.tag = 1;
        self.weightTextField.tag = 2;
    }
    return self;
}

+ (id)questionFormView
{
    EssayQuestionFormView *questionFormView = [[[NSBundle mainBundle] loadNibNamed:@"EssayQuestionForm" owner:nil options:nil] lastObject];
    
    if ([questionFormView isKindOfClass:[EssayQuestionFormView class]]) {
        return questionFormView;
    }
    else {
        return nil;
    }
}

- (void)handleDone:(id)sender
{
    
}

@end
