//
//  PickerQuestionFormView.h
//  LiveTime
//
//  Created by Jaehoon Lee on 2014. 5. 17..
//  Copyright (c) 2014년 DevTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickerQuestionFormView : UIView

@property (weak, nonatomic) IBOutlet UITextView *label;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UILabel *questionNumLabel;

+ (id)questionFormView;

@end
