//
//  ViewController.m
//  LiveTime
//
//  Created by Jaehoon Lee on 2014. 5. 9..
//  Copyright (c) 2014년 DevTeam. All rights reserved.
//

#import "ViewController.h"
#import "GADBannerView.h"

#define ADMOB_BANNER_UNIT_ID    @"ca-app-pub-9945504005389323/8774750295";      // 구글 애드몹 광고 유닛 아이디

@interface ViewController ()
{
        GADBannerView *_adBannerView;   // 구글 애드몹
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self showAdMobBanner];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 구글 애드몹 광고배너를 띄운다
- (void)showAdMobBanner
{
    // 기기의 해상도를 판별한다
    if (IS_4_INCH_DEVICE) {
        _adBannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, 518, 320, 50)];
    }
    else {
        _adBannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, 430, 320, 50)];
    }

    
    _adBannerView.adUnitID = ADMOB_BANNER_UNIT_ID;
    _adBannerView.rootViewController = self;
    [self.view addSubview:_adBannerView];
    
    GADRequest *request = [GADRequest request];
    //    request.testDevices = [NSArray arrayWithObject:@"d07aede00422e4aeed9eb93ebeeab19f"];
    
    [_adBannerView loadRequest:request];
}

@end
