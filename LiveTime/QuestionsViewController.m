//
//  QuestionsViewController.m
//  LiveTime
//
//  Created by Jaehoon Lee on 2014. 5. 10..
//  Copyright (c) 2014년 DevTeam. All rights reserved.
//

#import "QuestionsViewController.h"
#import "ObjectiveQuestionFormView.h"
#import "EssayQuestionFormView.h"
#import "PickerQuestionFormView.h"
#import "ResultViewController.h"

static NSString *CellIdentifier = @"Cell";


@interface QuestionsViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *presentPageNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *totlaPageNumLabel;

@property (weak, nonatomic) IBOutlet UIButton *prePageButton;
@property (weak, nonatomic) IBOutlet UIButton *nextPageButton;

@end


@implementation QuestionsViewController
{
    EssayQuestionFormView *_essayQuestionFormView;  // 주관식 문제 양식 클래스
    
    NSInteger _pageNum;         // 현재 페이지 번호
    
    NSMutableArray *_questions; // 질문들을 보관하는 배열
    NSMutableArray *_answers;   // 선택한 답에 따른 가감수명들을 보관하는 배열

    BOOL _selectedOdd;          // 홀수문제의 답을 선택했는지 여부
    BOOL _selectedEven;         // 짝수문제의 답을 선택했는지 여부
    
    NSInteger _selectedNumOdd;  // 홀수문제의 선택한 보기 번호
    NSInteger _selectedNumEven; // 짝수문제의 선택한 보기 번호

    NSDate *_birthDay;          // 생년월일
    NSInteger _height;          // 신장(cm)
    NSInteger _weight;          // 체중(kg)
    NSInteger _expectedAge;     // 기대수명(세)
    
    NSUserDefaults *_userData;  // 사용자 값(FirstVisit, ExpectedAge, BirthDay, DeathDay)
    
    NSString *_displayNameString;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 선택 답에 따른 가감수명을 보관하는 배열을 초기화한다
    _answers = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 27; i++) {
        [_answers addObject:@"empty"];
    }
    
    // 각종 변수들을 초기화 한다
    self.prePageButton.hidden = YES;
    self.nextPageButton.hidden = NO;
    _selectedOdd = NO;
    _selectedEven = NO;
    _selectedNumOdd = NSNotFound;
    _selectedNumEven = NSNotFound;
    
    _height = NSNotFound;
    _weight = NSNotFound;
    _pageNum = 0;
    
    // 문제들을 읽어와서 질문 뷰를 그려주고, 페이지 번호를 수정한다
    [self importQuestions];
    [self importQuestionForm];
    [self resetPageNumber];
}

// QuestionList.plist에서 문제들을 읽어온다
- (void)importQuestions
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"QuestionsList" ofType:@"plist"];
    _questions = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
}


#pragma mark - Core Logic

// QuestionForm 2개를 생성하여 배치한다
- (void)importQuestionForm
{
    NSInteger endNum;
    
    if (_pageNum == 13){
        endNum = _pageNum * 2;
    }
    else {
        endNum = _pageNum * 2 + 1;
    }
    
    for (NSInteger questionNum = _pageNum * 2; questionNum <= endNum; questionNum++) {
        
        NSString *question = [[_questions objectAtIndex:questionNum] valueForKey:@"question"];
        
        // 주관식 문제인 경우
        if ([[[_questions objectAtIndex:questionNum] objectForKey:@"type"] intValue] == 2) {
            // 주관식 문제 뷰를 생성 및 설정한다
            _essayQuestionFormView = [EssayQuestionFormView questionFormView];
            _essayQuestionFormView.frame = (questionNum % 2) ? CGRectMake(14, 300, 294, 187) : CGRectMake(14, 90, 294, 187);
            _essayQuestionFormView.questionNumLabel.text = [NSString stringWithFormat:@"%d", questionNum + 1];
            _essayQuestionFormView.label.text = question;
            
            // 키(cm)를 입력하는 텍스트필드의 키보드 상단에 "Done" 버튼을 생성한다
            UIToolbar *toolbarHeight = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
            toolbarHeight.barStyle = UIBarStyleDefault;
            UIBarButtonItem *doneButtonHeight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(handleDoneHeight:)];
            UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
            [toolbarHeight setItems:[NSArray arrayWithObjects:space, doneButtonHeight, nil]];
            _essayQuestionFormView.heightTextField.inputAccessoryView = toolbarHeight;
            [_essayQuestionFormView.heightTextField becomeFirstResponder];

            // 체중(kg)을 입력하는 텍스트필드의 키보드 상단에 "Done" 버튼을 생성한다
            UIToolbar *toolbarWeight = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
            toolbarWeight.barStyle = UIBarStyleDefault;
            UIBarButtonItem *doneButtonWeight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(handleDoneWeight:)];
            [toolbarWeight setItems:[NSArray arrayWithObjects:space, doneButtonWeight, nil]];
            _essayQuestionFormView.weightTextField.inputAccessoryView = toolbarWeight;
            
            // 텍스트필드의 좌우 여백을 설정한다
            UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 32)];
            _essayQuestionFormView.heightTextField.leftView = paddingView1;
            _essayQuestionFormView.heightTextField.leftViewMode = UITextFieldViewModeAlways;
            
            UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 32)];
            _essayQuestionFormView.weightTextField.leftView = paddingView2;
            _essayQuestionFormView.weightTextField.leftViewMode = UITextFieldViewModeAlways;
            
            UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35, 32)];
            _essayQuestionFormView.heightTextField.rightView = paddingView3;
            _essayQuestionFormView.heightTextField.rightViewMode = UITextFieldViewModeAlways;
            
            UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35, 32)];
            _essayQuestionFormView.weightTextField.rightView = paddingView4;
            _essayQuestionFormView.weightTextField.rightViewMode = UITextFieldViewModeAlways;
            
            _essayQuestionFormView.heightTextField.placeholder = NSLocalizedString(@"HEIGHT_PLACEHOLDER", @"키 입력 텍스트필드");
            _essayQuestionFormView.weightTextField.placeholder = NSLocalizedString(@"WEIGHT_PLACEHOLDER", @"키 입력 텍스트필드");
            
            // 텍스트필드의 델리게이트를 지정한다
            _essayQuestionFormView.heightTextField.delegate = self;
            _essayQuestionFormView.weightTextField.delegate = self;
            
            // 주관식 문제 뷰를 화면에 그린다
            [self.view addSubview:_essayQuestionFormView];
        }
        
        // 데이트 피커(생년월일) 문제인 경우
        else if ([[[_questions objectAtIndex:questionNum] objectForKey:@"type"] intValue] == 1) {
            // 데이트 피커 문제 뷰를 화면에 그린다
            PickerQuestionFormView *pickerQuestionFormView = [PickerQuestionFormView questionFormView];
            
            if (IS_4_INCH_DEVICE) {
                pickerQuestionFormView.frame = (questionNum % 2) ? CGRectMake(14, 300, 294, 187) : CGRectMake(14, 90, 294, 187);
            }
            else {
                pickerQuestionFormView.frame = (questionNum % 2) ? CGRectMake(14, 255, 294, 175) : CGRectMake(14, 70, 294, 175);
            }
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
            NSString *currentLanguage = [languages objectAtIndex:0];
            
            if ([currentLanguage isEqualToString:@"ko"]) {
                NSLog(@"한국어 사용자입니다.");
                pickerQuestionFormView.datePicker.locale = [NSLocale localeWithLocaleIdentifier:@"ko_KO"];
            }
            else if ([currentLanguage isEqualToString:@"ja"]) {
                NSLog(@"일본어 사용자입니다.");
                pickerQuestionFormView.datePicker.locale = [NSLocale localeWithLocaleIdentifier:@"ja_JP"];
            }
            else {
                NSLog(@"영어 사용자입니다.");
            }
            
            pickerQuestionFormView.questionNumLabel.text = [NSString stringWithFormat:@"%d", questionNum + 1];
            pickerQuestionFormView.label.text = question;
            [pickerQuestionFormView.datePicker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
            

            
            [self.view addSubview:pickerQuestionFormView];
        }
        
        // 객관식 문제인 경우
        else {
            // 객관식 문제 뷰를 생성 및 설정한다
            ObjectiveQuestionFormView *objectiveQuestionFormView = [ObjectiveQuestionFormView questionFormView];
            
            if (IS_4_INCH_DEVICE) {
                objectiveQuestionFormView.frame = (questionNum % 2) ? CGRectMake(14, 300, 294, 187) : CGRectMake(14, 90, 294, 187);
            }
            else {
                objectiveQuestionFormView.frame = (questionNum % 2) ? CGRectMake(14, 255, 294, 175) : CGRectMake(14, 70, 294, 175);
            }
            
            objectiveQuestionFormView.questionNumLabel.text = [NSString stringWithFormat:@"%d", questionNum + 1];
            objectiveQuestionFormView.label.text = question;
            
            // 객관식 문제의 테이블을 설정한다
            [objectiveQuestionFormView.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
            objectiveQuestionFormView.tableView.tag = questionNum % 2;
            
            // 테이블의 델리게이트를 지정한다
            objectiveQuestionFormView.tableView.dataSource = self;
            objectiveQuestionFormView.tableView.delegate = self;
            
            // 테이블의 높이를 보기의 개수에 맞게 자동으로 지정한다
//            CGFloat height = [[[_questions objectAtIndex:questionNum] objectForKey:@"answer"] count] * 44.0;
//            CGRect tableFrame = objectiveQuestionFormView.tableView.frame;
//            tableFrame.size.height = height;
//            objectiveQuestionFormView.tableView.frame = tableFrame;
            
            // 객관식 문제 뷰를 화면에 그린다
            [self.view addSubview:objectiveQuestionFormView];
        }
    }
}

// 두 번째 문제의 DatePicker 값(생년월일)을 변경한 경우
-(void)datePickerChanged:(id)sender
{
    // 변경한 날짜가 오늘로부터 과거인지 미래인지 검사한다
    NSDate *changedDate = (((UIDatePicker *)sender).date);
    NSDate *today = [NSDate date];
    NSComparisonResult compareResult = [changedDate compare:today];
    
    // 생년월일을 과거의 날짜로 알맞게 입력한 경우
    if (-1 == compareResult) {
        _birthDay = (((UIDatePicker *)sender).date);
        _answers[_pageNum * 2 + 1] = _birthDay;
        
        _selectedEven = YES;
    }
    // 생년월일을 미래의 날짜로 잘못 입력한 경우, 경고창을 띄운다
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", @"데이트피커 알림창 제목") message:NSLocalizedString(@"DATEPICKER_ALERT_MESSAGE", @"데이트피커 알림창 메세지") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BUTTON", @"데이트피커 알림창 버튼") otherButtonTitles: nil];
        [alert show];
        
        // 데이트 피커를 오늘 날짜로 되돌린다
        (((UIDatePicker *)sender).date) = today;
        
        _selectedEven = NO;
    }
}

// 마지막 문제의 키를 입력하는 키보드의 "Done" 버튼을 누르면
-(void)handleDoneHeight:(id)sender
{
    // 키보드를 사라지게 하고, 체중 값을 대입한다
    [_essayQuestionFormView.heightTextField resignFirstResponder];
    
    [self checkTextField];
}

// 마지막 문제의 체중을 입력하는 키보드의 "Done" 버튼을 누르면
-(void)handleDoneWeight:(id)sender
{
    // 키보드를 사라지게 하고, 체중 값을 대입한다
    [_essayQuestionFormView.weightTextField resignFirstResponder];
    
    [self checkTextField];
}

// 키와 체중 입력 여부를 체크한다
- (void)checkTextField
{
    _height = [_essayQuestionFormView.heightTextField.text intValue];
    _weight = [_essayQuestionFormView.weightTextField.text intValue];
    
    // 키의 입력 여부를 체크한다
    if (_height != NSNotFound && _height != 0) {
        _selectedOdd = YES;
    }
    else {
        _selectedOdd = NO;
    }
    
    // 체중의 입력 여부를 체크한다
    if (_weight != NSNotFound && _weight != 0) {
        _selectedEven = YES;
    }
    else {
        _selectedEven = NO;
    }
}

// 이전 문제로 넘어가는 버튼을 누르면
- (IBAction)previewButtonPressed:(id)sender
{
    // 첫 번째 페이지이면

    // 첫 번째 페이지가 아니면
    if (_pageNum != 0) {
        self.nextPageButton.hidden = NO;
        
        // 마지막 페이지이면 기존의 질문 뷰 1개를 제거한다
        if (_pageNum == 13) {
            [[self.view.subviews objectAtIndex:(self.view.subviews.count)-1] removeFromSuperview];
        }
        // 마지막 페이지가 아니면 기존의 질문 뷰 2개를 제거한다
        else {
            [[self.view.subviews objectAtIndex:(self.view.subviews.count)-1] removeFromSuperview];
            [[self.view.subviews objectAtIndex:(self.view.subviews.count)-1] removeFromSuperview];
        }
        
        // 페이지 번호를 감산한다
        _pageNum -= 1;
        
        if (_pageNum == 0) {
            self.prePageButton.hidden = YES;
        }
        
        // 새로운 질문 뷰를 그리고, 페이지 번호를 수정한다
        [self importQuestionForm];
        [self resetPageNumber];
    }
}

// 다음 문제로 넘어가는 버튼을 누르면
- (IBAction)nextQuestionPressed:(id)sender
{
    // 사용자가 답을 선택하지 않았으면 경고창을 띄운다
    if (_selectedOdd == NO || _selectedEven == NO) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", @"알림창 제목") message:NSLocalizedString(@"NO_ANSWER_ALERT_MESSAGE", @"알림창 메세지") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BUTTON", @"알림창 버튼") otherButtonTitles:nil];
            alert.tag = 2;
        [alert show];
        }
        
    // 사용자가 답을 선택했으면 다음 문제로 넘어간다
    else {
        // 기존의 질문 뷰를 제거한다
        [[self.view.subviews objectAtIndex:(self.view.subviews.count)-1] removeFromSuperview];
        [[self.view.subviews objectAtIndex:(self.view.subviews.count)-1] removeFromSuperview];
            
        // 각종 변수를 초기화한다
        _pageNum += 1;
        _selectedOdd= NO;
        _selectedEven = NO;
        _selectedNumOdd = NSNotFound;
        _selectedNumEven = NSNotFound;
        self.prePageButton.hidden = NO;
        if (_pageNum == 13) {
            self.nextPageButton.hidden = YES;
        }
            
        // 새로운 질문 뷰를 그리고, 페이지 번호를 수정한다
        [self importQuestionForm];
        [self resetPageNumber];
    }
}

// 결과 보기 버튼을 누르면 결과 페이지로 이동한다
- (IBAction)resultButtonPressed:(id)sender
{
    // 키와 체중을 입력하지 않았으면 경고창을 띄운다
    if (_selectedOdd == NO || _selectedEven == NO) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", @"알림창 제목") message:NSLocalizedString(@"NO_ANSWER_ALERT_MESSAGE", @"알림창 메세지") delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BUTTON", @"알림창 버튼") otherButtonTitles:nil];
        [alert show];
    }
    // 키와 체중을 입력했으면 결과 페이지로 이동한다
    else {
        // 질문 뷰를 제거한다
        [[self.view.subviews objectAtIndex:(self.view.subviews.count)-1] removeFromSuperview];
        
        // 최종으로 기대수명을 계산한다
        _answers[_pageNum * 2] = [NSNumber numberWithInteger: [self calculateObesity]];
        _expectedAge = [self calculateExpectedAge];
        
        // 사망일자를 계산한다
        NSDateComponents *components = [[NSDateComponents alloc]init];
        components.year = _expectedAge;
        NSDate *deathDay = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:_birthDay options:0];
        
        // 사용자 값을 저장한다
        _userData = [NSUserDefaults standardUserDefaults];
        [_userData setValue:[NSNumber numberWithInteger:0] forKey:@"FirstVisit"];
        [_userData setValue:[NSNumber numberWithInteger:_expectedAge] forKeyPath:@"ExpectedAge"];
        [_userData setValue:_birthDay forKey:@"BirthDay"];
        [_userData setValue:deathDay forKey:@"DeathDay"];
        [_userData synchronize];
        
        // 결과 화면으로 이동한다
        UIStoryboard *storyboard = self.storyboard;
        ResultViewController *resultView = (ResultViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ResultVC"];
        resultView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:resultView animated:YES completion:nil];
    }
}

// 화면의 페이지 번호를 변경한다
- (void)resetPageNumber
{
    NSString *presentPageNumStr = [NSString stringWithFormat:@"%d", _pageNum+1];
    [self.presentPageNumLabel setText:presentPageNumStr];
    
    NSString *totalPageNumStr = [NSString stringWithFormat:@"/ %d", [_questions count] / 2 + 1];
    [self.totlaPageNumLabel setText:totalPageNumStr];
}

// 비만도에 따른 가감수명 값을 얻는다
- (NSInteger)calculateObesity
{
    NSInteger normalWeight = (_height - 100) *0.9;
    NSInteger errorWeight = _weight - normalWeight;
    
    if (errorWeight >= 23) {
        return -8;
    }
    else if (13 <= errorWeight <= 22) {
        return -4;
    }
    else if (5 <= errorWeight <= 12) {
        return -2;
    }
    else {
        return 0;
    }
}

// 최종적으로 기대수명을 계산한다
- (NSInteger)calculateExpectedAge
{
    NSInteger finalAge = 0;
    
    for (int i = 0; i < [_answers count]; i++) {
        if (i != 1) {
            finalAge += [[_answers objectAtIndex:i] intValue];
        }
    }
    return finalAge;
}


#pragma mark - TableView Delegate

// 객관식 문제의 테이블 셀(보기) 개수를 설정한다
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // 홀수문제인 경우
    if (tableView.tag == 0) {
        return [[[_questions objectAtIndex:_pageNum * 2] objectForKey:@"answer"] count];
    }
    // 짝수문제인 경우
    else {
        return [[[_questions objectAtIndex:_pageNum * 2 + 1] objectForKey:@"answer"] count];
    }
}

// 객관식 문제의 테이블 셀(보기) 내용을 설정한다
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    // 테이블 셀의 높이를 구한다
    CGFloat height = tableView.rowHeight;
    
    // 홀수문제인 경우
    if (tableView.tag == 0) {
        // 보기의 내용을 표시한다
        NSArray *array = [[_questions objectAtIndex:_pageNum * 2] objectForKey:@"answer"];
        NSString *exampleString = [array objectAtIndex:indexPath.row];
        cell.textLabel.text = exampleString;
        
        // 보기의 개수를 근거로 테이블의 총 높이를 구한다
        height *= [[[_questions objectAtIndex:_pageNum * 2] objectForKey:@"answer"] count];
    }
    
    // 짝수문제인 경우
    else {
        // 보기의 내용을 표시한다
        NSArray *array = [[_questions objectAtIndex:_pageNum * 2 + 1] objectForKey:@"answer"];
        NSString *exampleString = [array objectAtIndex:indexPath.row];
        cell.textLabel.text = exampleString;

        // 보기의 개수를 근거로 테이블의 총 높이를 구한다
        height *= [[[_questions objectAtIndex:_pageNum * 2 + 1] objectForKey:@"answer"] count];
    }

    // 테이블의 높이와 셀 간 경계선을 설정한다
    CGRect tableFrame = tableView.frame;
    tableFrame.size.height = height;
    tableView.frame = tableFrame;
    tableView.separatorColor = [UIColor clearColor];
    
    // 문제 번호 이미지를 삽입한다
    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"question_check_gray.png"]];
    cell.imageView.image = image;
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

// 객관식 문제의 테이블 셀(보기)을 선택하면 체크 표시를 하고 가감수명을 배열에 저장한다
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *values = [[NSMutableArray alloc] init];
    
    // 홀수문제인 경우
    if (tableView.tag == 0) {
        // 해당문제의 각 보기에 따른 가감수명 값들을 읽어온다
        values = [[_questions objectAtIndex:_pageNum * 2] objectForKey:@"value"];
        
        // 사용자가 선택한 보기의 셀이 아니면(= 이전에 선택한 셀과 다른 셀을 선택하면)
        if (indexPath.row != _selectedNumOdd) {
            // 이전에 선택했던 셀이 있는 경우 체크 표시를 제거한다
            if (_selectedNumOdd != NSNotFound) {
                NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:_selectedNumOdd inSection:0];
                UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:oldIndexPath];
                oldCell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"question_check_gray.png"]];
                oldCell.textLabel.textColor = [UIColor blackColor];
            }
            // 사용자가 선택한 보기 번호를 저장한다
            _selectedNumOdd = indexPath.row;

            // 선택한 보기의 셀에 체크 표시가 되도록 설정한다
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"question_check_red.png"]];
            cell.textLabel.textColor = [UIColor redColor];
        }
        // 선택한 보기에 따른 가감수명을 배열에 저장한다
        _answers[_pageNum * 2] = [values objectAtIndex:indexPath.row];
        
        _selectedOdd = YES;
    }
    
    // 짝수문제인 경우
    else {
        // 해당문제의 각 보기에 따른 가감수명 값들을 읽어온다
        values = [[_questions objectAtIndex:_pageNum * 2 + 1] objectForKey:@"value"];
        
        // 사용자가 선택한 보기의 셀이 아니면(= 이전에 선택한 셀과 다른 셀을 선택하면)
        if (indexPath.row != _selectedNumEven) {
            // 이전에 선택했던 셀이 있는 경우 체크 표시를 제거한다
            if (_selectedNumEven != NSNotFound) {
                NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:_selectedNumEven inSection:0];
                UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:oldIndexPath];
                oldCell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"question_check_gray.png"]];
                oldCell.textLabel.textColor = [UIColor blackColor];
            }
            // 사용자가 선택한 보기 번호를 저장한다
            _selectedNumEven = indexPath.row;

            // 선택한 보기에 체크 표시가 되도록 설정한다
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"question_check_red.png"]];
            cell.textLabel.textColor = [UIColor redColor];
        }

        // 선택한 보기에 따른 가감수명을 배열에 저장한다
        _answers[_pageNum * 2 + 1] = [values objectAtIndex:indexPath.row];

        _selectedEven = YES;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

// 객관식 문제의 테이블 셀(보기) 높이를 설정한다
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 홀수문제인 경우
    if (tableView.tag == 0) {
        if (2 == [[[_questions objectAtIndex:_pageNum * 2] objectForKey:@"answer"] count]) {
            tableView.frame = CGRectMake(29, 87, 240, 167);
            return 40;
        }
        else if (3 == [[[_questions objectAtIndex:_pageNum * 2] objectForKey:@"answer"] count]) {
            return 35;
        }
        else {
            return 28;
        }
    }
    // 짝수문제인 경우
    else {
        if (2 == [[[_questions objectAtIndex:_pageNum * 2+1] objectForKey:@"answer"] count]) {
            tableView.frame = CGRectMake(29, 87, 240, 167);
            return 40;
        }
        else if (3 == [[[_questions objectAtIndex:_pageNum * 2+1] objectForKey:@"answer"] count]) {
            return 35;
        }
        else {
            return 28;
        }
    }
    
    return 28;
}

// 상태바의 글씨 색상을 설정한다
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end