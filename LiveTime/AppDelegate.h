//
//  AppDelegate.h
//  LiveTime
//
//  Created by Jaehoon Lee on 2014. 5. 9..
//  Copyright (c) 2014년 DevTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
