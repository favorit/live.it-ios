//
//  QuestionForm.m
//  LiveTime
//
//  Created by Jaehoon Lee on 2014. 5. 16..
//  Copyright (c) 2014년 DevTeam. All rights reserved.
//

#import "ObjectiveQuestionFormView.h"


@implementation ObjectiveQuestionFormView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

    }
    return self;
}

+ (id)questionFormView
{
    ObjectiveQuestionFormView *questionFormView = [[[NSBundle mainBundle] loadNibNamed:@"ObjectiveQuestionForm" owner:nil options:nil] lastObject];
    
    if ([questionFormView isKindOfClass:[ObjectiveQuestionFormView class]]) {
        return questionFormView;
    }
    else {
        return nil;
    }
}


@end
