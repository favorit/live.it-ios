//
//  PickerQuestionFormView.m
//  LiveTime
//
//  Created by Jaehoon Lee on 2014. 5. 17..
//  Copyright (c) 2014년 DevTeam. All rights reserved.
//

#import "PickerQuestionFormView.h"

@implementation PickerQuestionFormView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        

    }
    
    return self;
}

+ (id)questionFormView
{
    PickerQuestionFormView *questionFormView = [[[NSBundle mainBundle] loadNibNamed:@"PickerQuestionForm" owner:nil options:nil] lastObject];
    
    if ([questionFormView isKindOfClass:[PickerQuestionFormView class]]) {
        return questionFormView;
    }
    else {
        return nil;
    }
}



@end
