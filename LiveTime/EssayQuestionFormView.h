//
//  EssayQuestionFormView.h
//  LiveTime
//
//  Created by Jaehoon Lee on 2014. 5. 16..
//  Copyright (c) 2014년 DevTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EssayQuestionFormView : UIView

@property (weak, nonatomic) IBOutlet UITextView *label;
@property (weak, nonatomic) IBOutlet UITextField *heightTextField;
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;
@property (weak, nonatomic) IBOutlet UILabel *questionNumLabel;

+ (id)questionFormView;

@end
