//
//  QuestionForm.h
//  LiveTime
//
//  Created by Jaehoon Lee on 2014. 5. 16..
//  Copyright (c) 2014년 DevTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ObjectiveQuestionFormView : UIView

@property (weak, nonatomic) IBOutlet UITextView *label;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *questionNumLabel;

+ (id)questionFormView;

@end
